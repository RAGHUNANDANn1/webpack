# Webpack

In this repo i document my journey of learning webpack.

## Generate default package json

npm init -y

## Install webpack and webpack cli

npm install webpack webpack-cli --save-dev

## Run webpack with default configuration

npx webpack

## Run webpack with detailed stats

npx webpack --stats detailed

## Install style-loader and css-loader for loading css in webpack

npm install css-loader style-loader --save-dev

## Using scss with webpack

npm install sass-loader sass

## Use babel and plugins

npm install @babel/core babel-loader @babel/preset-env @babel/plugin-proposal-class-properties

## Reduce bundle size

bundle size of webpack can be reduced using tercer plugin which comes default with webpack 5

## Extract styles into seperate bundle

npm install mini-css-extract-plugin --save-dev

we can use this plugin to extract styles to different bundle and load them seperately

## Cache js and css

browsers identify file changes using filename, this can be utitlised for caching and we can add MD4 cache to our file name.MD4 cache changes only if there is a change in the filename

## Clean dist folder on every build

npm i clean-webpack-plugin --save-dev
this plugin runs before evrey build and makes sure that old bundles are deleted

## Automatically generate index.html with md4 file names of js and css references

npm install html-webpack-plugin --save-dev

## Use custom template for generating index,html

npm i handlebars-loader --save-dev
npm i --save-dev handlebars

## Webpack dev server for hot reloading

npm install webpack-dev-server --save-dev
hot reloads js and css mainf better dev ex
