import Heading from "./components/heading/heading";
import MangoImage from "./components/mango/mango-image";

const heading = new Heading();
heading.render("mango");

const mango = new MangoImage();
mango.render();

import("HelloWorldApp/HelloWorldButton").then((HelloWorldButtonModule) => {
  const HelloWorldButton = HelloWorldButtonModule.default;
  const helloWorldButton = new HelloWorldButton();
  helloWorldButton.render();
});
