import Mango from "./mango.jpeg";
import "./mango-image.scss";

class MangoImage {
  render() {
    const img = document.createElement("img");
    img.src = Mango;
    img.alt = "Mango";
    img.classList.add("mango-image");

    const body = document.querySelector("body");
    body.appendChild(img);
  }
}

export default MangoImage;
